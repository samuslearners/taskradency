const ls =  [51, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60, 56, 58, 59, 61, 60];
const k = 7;
const t = 387;

function combinationSum(ls, k, start, t) {
  const sums = new Set();
  for(let i = start; i < ls.length; i++){
    if(ls[i] > t) {
      continue;
    }
    if(k === 1) {
      sums.add(ls[i]);
    } else {
      const generated = combinationSum(ls, k-1, i+1, t-ls[i]);
      for (let n of generated) {
        sums.add(n+ls[i]);
      }
    }
  }
  return sums;
}

const chooseBestDistance = (t, k, ls) => {
  if(k >= ls.length) {
    return null;
  }

  const sorted = [...ls];
  sorted.sort();

  let minSum = 0;
  let maxSum = 0;
  for(let i = 0; i < k; i++) {
    minSum += sorted[i];
    maxSum += sorted[sorted.length-1-i];
  }

  if(t < minSum ) {
    return null;
  }

  if(t = minSum) {
    return minSum;
  }
  
  if(t >= maxSum){
    return maxSum;
  }

  sorted.reverse();
  const sums = combinationSum(sorted, k, 0, t);
  maxSum = 0;

  for (let sum of sums) {
    if(maxSum < sum && sum <= t){
      maxSum = sum;
    }
  }

  return maxSum;
}

console.log(chooseBestDistance(t, k, ls));